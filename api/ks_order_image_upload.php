<?php 

include 'response.php';
include 'opendb.php';
$heder = getallheaders();
$inputPost = filter_input_array(INPUT_POST);
$DeviceId = $heder["DeviceId"];
$Plateform = $heder["Plateform"];
$Version = $heder["Version"];
$userId=$inputPost["userId"];
$orderId=$inputPost["orderId"];
$uniqueId=$inputPost["uniqueId"];
$imgType=$inputPost["imgType"];
$createdAt=$inputPost["createdAt"];
$actualImageName=$inputPost["actualImageName"];

	//importing dbDetails file 
	require_once 'dbDetails.php';
	
	//this is our upload folder 
	$upload_path = 'ks_order_images/';
	
	//Getting the server ip 
	$server_ip = "www.realsoftware.co.in";//(gethostname());
	
	//creating the upload url 
	$upload_url = 'http://'.$server_ip.'/api/'.$upload_path; 
	
	//response array 
	$response = array(); 
	
	
	if($_SERVER['REQUEST_METHOD']=='POST'){
		
		//checking the required parameters from the request 
		if(isset($_POST['orderId']) and isset($_FILES['image']['name'])){
			
			//connecting to the database 
			$con = mysqli_connect(HOST,USER,PASS,DB) or die('Unable to Connect...');
			
			
			$Actualfilename=$_FILES['image']['name'];
			
			//getting file info from the request 
			$fileinfo = pathinfo($_FILES['image']['name']);
			
			//getting the file extension 
			$extension = $fileinfo['extension'];
			
			
			//file url to store in the database 
			$file_url = $upload_url .$userId .$orderId. '.' . $extension;
			
			//file path to upload in the server 
			$file_path = $upload_path . $userId.$orderId. '.'. $extension; 
			
			//trying to save the file in the directory 
			try{
				//saving the file 
				move_uploaded_file($_FILES['image']['tmp_name'],$file_path);
			
				$sql = "UPDATE ks_neworders SET imagePath='$file_url' , imgStatus='1', imgType='$imgType',actualImageName='$actualImageName', createdAt='$createdAt' WHERE userId='$userId' and orderId='$orderId'";
				
				
				$query = mysqli_query($con, "SELECT * from ks_neworders WHERE orderId='$orderId'");
$row = mysqli_num_rows($query);
if ($row >= 1) {
				//adding the path and name to database 
				if(mysqli_query($con,$sql)){
					
					//filling response array with values 
					$response['error'] = false; 
					$response['image'] = $file_url; 
					$response['orderId'] = $orderId;
				}
}
else
{
 	$response['error']=true;
			$response['message']='OrderId Not exits.';   
}
			//if some error occurred 
			}catch(Exception $e){
				$response['error']=true;
				$response['message']=$e->getMessage();
			}		
			//displaying the response 
			echo json_encode($response);
			
			//closing the connection 
			mysqli_close($con);
	

		    
		}else{
			$response['error']=true;
			$response['message']='Please choose a file';
		}
	}
	


?>
