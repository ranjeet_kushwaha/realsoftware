<?php 

include 'response.php';
include 'opendb.php';
$heder = getallheaders();
$inputPost = filter_input_array(INPUT_POST);
$DeviceId = $heder["DeviceId"];
$Plateform = $heder["Plateform"];
$Version = $heder["Version"];
$userId=$inputPost["userId"];
$orderId=$inputPost["orderId"];
	
	//importing dbDetails file 
	require_once 'dbDetails.php';
	
	//this is our upload folder 
	$upload_path = 'ks_user_profile/';
	
	//Getting the server ip 
	$server_ip = "www.realsoftware.co.in";//(gethostname());
	
	//creating the upload url 
	$upload_url = 'http://'.$server_ip.'/api/'.$upload_path; 
	
	//response array 
	$response = array(); 
	
	
	if($_SERVER['REQUEST_METHOD']=='POST'){
		
		//checking the required parameters from the request 
		if(isset($_POST['name']) and isset($_FILES['image']['name'])){
			
			//connecting to the database 
			$con = mysqli_connect(HOST,USER,PASS,DB) or die('Unable to Connect...');
			
			//getting name from the request 
			$name = $_POST['name'];
			
			$Actualfilename=$_FILES['image']['name'];
			
			//getting file info from the request 
			$fileinfo = pathinfo($_FILES['image']['name']);
			
			//getting the file extension 
			$extension = $fileinfo['extension'];
			
			
			//file url to store in the database 
			$file_url = $upload_url .$userId . '.' . $extension;
			
			//file path to upload in the server 
			$file_path = $upload_path . $userId. '.'. $extension; 
			
			//trying to save the file in the directory 
			try{
				//saving the file 
				move_uploaded_file($_FILES['image']['tmp_name'],$file_path);
				/*$sql = "INSERT INTO ks_users_photo (`id`, `image`, `tags`,`userId`,`img_actual_name`) VALUES (NULL, '$file_url', '$name','$userId','$Actualfilename');";*/
				
				$sql = "UPDATE user SET imagePath='$file_url' WHERE userId='$userId' and orderId='$orderId'";
				
				//adding the path and name to database 
				if(mysqli_query($con,$sql)){
					
					//filling response array with values 
					$response['error'] = false; 
					$response['image'] = $file_url; 
					$response['tags'] = $name;
				}
			//if some error occurred 
			}catch(Exception $e){
				$response['error']=true;
				$response['message']=$e->getMessage();
			}		
			//displaying the response 
			echo json_encode($response);
			
			//closing the connection 
			mysqli_close($con);
		}else{
			$response['error']=true;
			$response['message']='Please choose a file';
		}
	}
	
	/*
		We are generating the file name 
		so this method will return a file name for the image to be upload 
	*/
	function GenUserId($length = 4) {
    $chars = '0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .=substr($chars, $index, 1);
    }
    return $result;
}

	/*function getFileName($uniqeId){
		$con = mysqli_connect(HOST,USER,PASS,DB) or die('Unable to Connect...');
		$sql ="SELECT max(id) as id FROM ks_users_photo";
		$result = mysqli_fetch_array(mysqli_query($con,$sql));
	
	mysqli_close($con);
		if($result['id']==null)
			return $uniqeId.'_1'; 
		else 
			return $uniqeId.'_'.$result['id'];  
			//return $uniqeId.'_'.$result['id']; 
	}*/
?>
